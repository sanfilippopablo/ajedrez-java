package entidades;

public class Reina extends Pieza {
	
	public Reina(int posX, int posY, Jugador jugador, String id) {
		super(posX, posY, jugador,id);
	}

	public Reina() {
		// TODO Auto-generated constructor stub
	}

	public boolean esMovimientoValido(int posXDestino, int posYDestino) {
		int diffX = Math.abs(posXDestino - posX);
		int diffY = Math.abs(posYDestino - posY);
		
		boolean movimientoDiagonal = diffX == diffY;
		boolean movimientoLineaRecta = posXDestino == posX || posYDestino == posY;
		
		return movimientoDiagonal || movimientoLineaRecta;
	}
}
