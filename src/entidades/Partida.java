package entidades;

import java.util.ArrayList;

import datos.ColeccionPartidas;
import errores.ValidationError;

public class Partida {
	int id;
	Jugador jugador1;
	Jugador jugador2;
	Jugador turnoActual;
	boolean terminada = false;

	ArrayList<Pieza> piezas = new ArrayList<Pieza>();
	
	public Partida() {
	
	}
	
	public void inicializarPiezas() {
		piezas.add(new Pieza(1, 1, this.jugador1, "T1"));
		piezas.add(new Torre(8, 1, this.jugador1, "T2"));
		piezas.add(new Caballo(2, 1, this.jugador1, "C1"));
		piezas.add(new Caballo(7, 1, this.jugador1, "C2"));
		piezas.add(new Alfil(3, 1, this.jugador1, "A1"));
		piezas.add(new Alfil(6, 1, this.jugador1, "A2"));
		piezas.add(new Reina(4, 1, this.jugador1, "D"));
		piezas.add(new Rey(5, 1, this.jugador1,"R"));
		
		for (int i = 1; i <= 8; i++) {
			piezas.add(new Peon(i, 2, this.jugador1, "P" + i));
		}
		
		// Inicializar piezas del jugador 2
		piezas.add(new Torre(1, 8, this.jugador2, "T1"));
		piezas.add(new Torre(8, 8, this.jugador2, "T2"));
		piezas.add(new Caballo(2, 8, this.jugador2, "C1"));
		piezas.add(new Caballo(7, 8, this.jugador2, "C2"));
		piezas.add(new Alfil(3, 8, this.jugador2, "A1"));
		piezas.add(new Alfil(6, 8, this.jugador2, "A2"));
		piezas.add(new Reina(5, 8, this.jugador2, "D"));
		piezas.add(new Rey(4, 8, this.jugador2, "R"));
		
		for (int i = 1; i <= 8; i++) {
		piezas.add(new Peon(i, 7, this.jugador2, "P" + i));
		}
	}

	public void mover(Pieza pieza, int posXDestino, int posYDestino) throws ValidationError {
		ColeccionPartidas cp = new ColeccionPartidas();
		
		if (!(posXDestino > 0 && posXDestino <= 8 && posYDestino > 0 && posYDestino <=8)) {
			throw new ValidationError("La posición de destino está fuera del tablero.");
		}
		
		// Si había pieza propia, error
		for (Pieza pieza2 : piezas) {
			
			
			if ((pieza2.getPosX() == posXDestino && pieza2.getPosY() == posYDestino) && pieza2.getJugador() == pieza.getJugador()) {
				throw new ValidationError("Hay una pieza propia ahí.");
			}
		}
		
		// Validar si es un movimiento válido para esa pieza
		if (pieza.getId().charAt(0) == 'P'){
			boolean arriba = jugador1.getdNI() == turnoActual.getdNI();
			
			boolean haceUnoAdelante, haceUnoEnDiagonal;
			if (arriba) {
				haceUnoAdelante = posXDestino == pieza.getPosX() && posYDestino - pieza.getPosY() == 1;
				haceUnoEnDiagonal = Math.abs(posXDestino - pieza.getPosX()) == 1 && posYDestino - pieza.getPosY() == 1;
				
			}
			else {
				haceUnoAdelante = posXDestino == pieza.getPosX() && posYDestino - pieza.getPosY() == -1;
				haceUnoEnDiagonal = Math.abs(posXDestino - pieza.getPosX()) == 1 && posYDestino - pieza.getPosY() == 1;
			}
			System.out.println("What");
			
			if (haceUnoAdelante) {
				System.out.println("Hacia adelante");
				// Hay una pieza enfrente?
				for (Pieza pieza2 : piezas) {
					if (pieza2.getPosX() == posXDestino && pieza2.getPosY() == posYDestino) {
						// Hay pieza enfrente. Movimiento inválido.
						throw new ValidationError("Movimiento no válido.");
					}
				}
				// No hay pieza en frente. Movimiento válido.
			}
			else if (haceUnoEnDiagonal) {
				// El movimiento es válido sólo si ya hay una pieza ahí
				boolean hay = false;
				for (Pieza pieza2 : piezas) {
					if (pieza2.getPosX() == posXDestino && pieza2.getPosY() == posYDestino) {
						hay = true;
						break;
					}
				}
				if (!hay) {
					throw new ValidationError("Movimiento no válido.");
				}
			}
				
		}
		else {
			if (!pieza.esMovimientoValido(posXDestino, posYDestino)) {
				throw new ValidationError("Movimiento inválido.");
			}
		}

		// NO HAY ERROR
		
		// Si hay pieza del otro, comerla
		for (Pieza pieza2 : piezas) {
			if (pieza2.getPosX() == posXDestino && pieza2.getPosY() == posYDestino) {
				
				if (pieza2.getId().charAt(0) == 'R') {
					// Comieron al rey! FIN.
					terminada = true;
				}
				
				piezas.remove(pieza2);
				break;
			}
		}
		
		pieza.mover(posXDestino, posYDestino);
		
		// Update en DB
		cp.updatePartida(this);
		
		cambiarTurno();
	}
	
	public boolean estaTerminada() {
		return terminada;
	}

	public void cambiarTurno() {
		if (turnoActual == jugador1) {
			turnoActual = jugador2;
		}
		else {
			turnoActual = jugador1;
		}
	}


	public int getId() {
		// TODO Auto-generated method stub
		return id;
	}

	public void setJugador1(Jugador jugador1) {
		this.jugador1 = jugador1;
	}


	public void setJugador2(Jugador jugador2) {
		this.jugador2 = jugador2;
	}


	public void setTurnoActual(Jugador turnoActual) {
		this.turnoActual = turnoActual;
	}


	public void setPiezas(ArrayList<Pieza> piezas) {
		this.piezas = piezas;
	}


	public Jugador getJugador1() {
		// TODO Auto-generated method stub
		return jugador1;
	}
	
	public Jugador getJugador2(){
		return jugador2;
	}


	public void setId(int id) {
		// TODO Auto-generated method stub
		this.id = id;
	}


	public Jugador getTurnoActual() {
		return turnoActual;
	}
	
	public ArrayList<Pieza> getPiezas(){
		return piezas;
	}
	
	public Pieza getPiezaById(String id) {
		for (Pieza pieza : piezas) {
			
			if (pieza.getId().equals(id) && pieza.getJugador().getdNI() == this.getTurnoActual().getdNI()) {
				return pieza;
			}
		}
		
		return null;
	}
}
