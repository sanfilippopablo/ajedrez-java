package entidades;

public class Jugador {
	int dNI;
	String nombre;
	String apellido;
	
	public int getdNI() {
		return dNI;
	}
	public void setdNI(int dNI) {
		this.dNI = dNI;
	}
	public void setNombre(String nombre) {
		this.nombre = nombre;
	}
	public void setApellido(String apellido) {
		this.apellido = apellido;
	}
	public String getNombre() {
		return nombre;
	}
	public String getApellido() {
		return apellido;
	}

}
