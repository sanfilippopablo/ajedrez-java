package entidades;

public class Alfil extends Pieza {
	
	public Alfil() {
		super();
	}

	public Alfil(int posX, int posY, Jugador jugador, String id) {
		super(posX, posY, jugador,id);
	}

	public boolean esMovimientoValido(int posXDestino, int posYDestino) {
		return Math.abs(posXDestino - posX) == Math.abs(posYDestino - posY);
	}
}
