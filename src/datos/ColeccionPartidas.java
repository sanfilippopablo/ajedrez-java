package datos;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;

import entidades.Jugador;
import entidades.Partida;
import entidades.Pieza;
import errores.ValidationError;

public class ColeccionPartidas {

	public Partida crearNuevaPartida(Jugador j1, Jugador j2) {

		Partida partida = new Partida();

		partida.setJugador1(j1);
		partida.setJugador2(j2);
		partida.setTurnoActual(j1);
		partida.inicializarPiezas();

		ResultSet rs = null;
		PreparedStatement stmt = null;
		Connection conn = null;

		try {
			conn = FabricaConexion.getInstancia().getConn();
			conn.setAutoCommit(false);

			stmt = conn.prepareStatement(
				"insert into partidas(jugador1, jugador2, turno) values (?,?,?)",PreparedStatement.RETURN_GENERATED_KEYS
			);

			stmt.setInt(1, j1.getdNI());
			stmt.setInt(2, j2.getdNI());
			stmt.setInt(3, j1.getdNI());

			stmt.execute();
			rs=stmt.getGeneratedKeys();

			if (rs != null && rs.next()) {
				partida.setId(rs.getInt(1));
			}

			stmt.close();
			rs.close();

			ArrayList<Pieza> piezas = partida.getPiezas();


			String query = "";
			for (int i = 0; i < piezas.size(); i++) {
				query += "INSERT INTO piezas(id, jugador, partida, posx, posy) VALUES (?, ?, ?, ?, ?);\n";
			}

			stmt = conn.prepareStatement(query);

			int i = 0;
			for (Pieza pieza: piezas) {
				stmt.setString(i * 5 + 1, pieza.getId());
				stmt.setInt(i * 5 + 2, pieza.getJugador().getdNI());
				stmt.setInt(i * 5 + 3, partida.getId());
				stmt.setInt(i * 5 + 4, pieza.getPosX());
				stmt.setInt(i * 5 + 5, pieza.getPosY());
				i++;
			}

			stmt.execute();

			conn.commit();
			conn.setAutoCommit(true);

		} catch (SQLException e) {
			try {
				e.printStackTrace();
				conn.rollback();
			} catch (SQLException e1) {
				// TODO Auto-generated catch block
				e1.printStackTrace();
			}
		}
		finally{

			try {
				if(rs!=null ) rs.close();
				if(stmt != null) stmt.close();
			} catch (SQLException e) {
				e.printStackTrace();
			}
			FabricaConexion.getInstancia().releaseConn();
		}
		return partida;
	}

	public ArrayList<Partida> getPartidas(int dni1, int dni2){
		ArrayList<Partida> partidas = new ArrayList<Partida>();
		try {
			Connection c = FabricaConexion.getInstancia().getConn();
			PreparedStatement s = c.prepareStatement("SELECT id FROM partidas WHERE (jugador1 = ? AND jugador2 = ?) OR (jugador1 = ? AND jugador2 = ?); ");
			s.setInt(1, dni1);
			s.setInt(2, dni2);
			s.setInt(3, dni2);
			s.setInt(4, dni1);
			ResultSet results = s.executeQuery();
			while (results.next()) {
				int id = results.getInt("id");
				Partida partida = getPartidaById(id);
				partidas.add(partida);
			}

			return partidas;
		} catch (Exception e) {
			e.printStackTrace();
			// TODO: handle exception
			return null;
		}

	}

	public void updatePartida (Partida p){
		PreparedStatement stmt = null;
		Connection conn = null;

		try {
			conn = FabricaConexion.getInstancia().getConn();
			conn.setAutoCommit(false);
			
			// Actualizar turno en la partida
			stmt = conn.prepareStatement("UPDATE partidas SET turno = ? WHERE id = ?;");
			stmt.setInt(1, p.getTurnoActual().getdNI());
			stmt.setInt(2, p.getId());
			stmt.execute();
			stmt.close();

			stmt = conn.prepareStatement("DELETE FROM piezas WHERE partida = ?;");
			stmt.setInt(1, p.getId());
			stmt.execute();
			stmt.close();

			for (int i = 0; i < p.getPiezas().size(); i++) {
				
				
				stmt = conn.prepareStatement("INSERT INTO piezas(partida, id, jugador, posx, posy) VALUES (?, ?, ?, ?, ?);");
				stmt.setInt(1, p.getId());
				stmt.setString(2, p.getPiezas().get(i).getId());
				stmt.setInt(3, p.getPiezas().get(i).getJugador().getdNI());
				stmt.setInt(4, p.getPiezas().get(i).getPosX());
				stmt.setInt(5, p.getPiezas().get(i).getPosY());
				stmt.execute();
				stmt.close();

			}
			
			conn.commit();
			conn.setAutoCommit(true);

		} catch (SQLException e) {
			e.printStackTrace();
			try {
				conn.rollback();
			} catch (SQLException e1) {
				// TODO Auto-generated catch block
				e1.printStackTrace();
			}
		}
	}

	public Partida getPartidaById(int id) throws ValidationError{
		ColeccionJugadores cj = new ColeccionJugadores();
		FactoryPiezas fp = new FactoryPiezas();

		Partida p = new Partida();
		try {
			Connection c = FabricaConexion.getInstancia().getConn();
			PreparedStatement s = c.prepareStatement("SELECT id, jugador1, jugador2, turno FROM partidas WHERE id = ? ");
			s.setInt(1, id);
			ResultSet results = s.executeQuery();

			if (results.next()) {
				p.setJugador1(cj.getJugadorByDNI(results.getInt("jugador1")));
				p.setJugador2(cj.getJugadorByDNI(results.getInt("jugador2")));
				p.setTurnoActual(cj.getJugadorByDNI(results.getInt("turno")));
				p.setId(results.getInt("id"));
			
				s.close();
				results.close();
	
				s = c.prepareStatement("SELECT * FROM piezas WHERE partida = ? ");
				s.setInt(1, id);
				results = s.executeQuery();
	
				ArrayList<Pieza> piezas = new ArrayList<Pieza>();
				while (results.next()) {
					Pieza pi = fp.piezaById(results.getString("id"));
					pi.setJugador(cj.getJugadorByDNI(results.getInt("jugador")));
					pi.setPosX(results.getInt("posx"));
					pi.setPosY(results.getInt("posy"));
					piezas.add(pi);
				}
				s.close();
				results.close();
	
				p.setPiezas(piezas);
	
				return p;
			}
			else{
				return null;
			}
		} catch (Exception e) {
			throw new ValidationError("Error con la conexion con la base de datos,"
					+ " vuelva a intentarlo.");
		}
	}
	
	public void eliminarPartida (Partida p) {
		PreparedStatement stmt = null;
		Connection conn = null;

		try {
			conn = FabricaConexion.getInstancia().getConn();
			conn.setAutoCommit(false);
			
			// Eliminar piezas
			stmt = conn.prepareStatement("DELETE FROM piezas WHERE partida = ?;");
			stmt.setInt(1, p.getId());
			stmt.execute();
			stmt.close();
			
			// Eliminar partida
			stmt = conn.prepareStatement("DELETE FROM partidas WHERE id = ?;");
			stmt.setInt(1, p.getId());
			stmt.execute();
			stmt.close();
			
			conn.commit();
			conn.setAutoCommit(true);

		} catch (SQLException e) {
			e.printStackTrace();
			try {
				conn.rollback();
			} catch (SQLException e1) {
				// TODO Auto-generated catch block
				e1.printStackTrace();
			}
		}
	}
}
