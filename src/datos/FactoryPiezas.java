package datos;

import entidades.*;

public class FactoryPiezas {
	public Pieza piezaById(String id) throws Exception {
		Pieza p = null;
		switch (id.charAt(0)) {
		case 'R':
			p = new Rey();
			p.setId(id);
			break;

		case 'C':
			p = new Caballo();
			p.setId(id);
			break;

		case 'A':
			p = new Alfil();
			p.setId(id);
			break;

		case 'D':
			p = new Reina();
			p.setId(id);
			break;

		case 'T':
			p = new Torre();
			p.setId(id);
			break;

		case 'P':
			p = new Peon();
			p.setId(id);
			break;

		default:
			throw new Exception("WTF. Wrong ID.");
		}
		return p;
	}
}
