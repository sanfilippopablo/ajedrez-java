package datos;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;

import entidades.Jugador;

public class ColeccionJugadores {
	
	public Jugador getJugadorByDNI(int DNI) {
		try {
			Connection c = FabricaConexion.getInstancia().getConn();
			PreparedStatement s = c.prepareStatement("SELECT * FROM jugadores WHERE dni = ?");
			s.setInt(1, DNI);
			ResultSet results = s.executeQuery();
			if (results.next()) {
				Jugador j = new Jugador();
				j.setdNI(results.getInt("dni"));
				j.setNombre(results.getString("nombre"));
				j.setApellido(results.getString("apellido"));
				return j;
			}
			else {
				return null;
			}
		} catch (Exception e) {
			// TODO: handle exception
			return null;
		}
		
	}

}
