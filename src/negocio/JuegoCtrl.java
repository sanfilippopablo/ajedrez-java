package negocio;

import datos.ColeccionPartidas;
import entidades.Partida;
import errores.ValidationError;

public class JuegoCtrl {

	public void moverPieza(Partida partida, String idPieza, String pX, String pY) throws Exception {
		int posX, posY;
		try {
			posX = Integer.parseInt(pX);
			posY = Integer.parseInt(pY);
		} catch (Exception e) {
			throw new ValidationError("Posición final no válida.");
		}
		partida.mover(partida.getPiezaById(idPieza), posX, posY);
		
		
		// Eliminar partida si terminó
		if (partida.estaTerminada()) {
			ColeccionPartidas cp = new ColeccionPartidas();
			cp.eliminarPartida(partida);
		}
	}

	
}
