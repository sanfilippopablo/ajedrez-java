package negocio;

import java.util.ArrayList;

import datos.ColeccionJugadores;
import datos.ColeccionPartidas;
import entidades.Jugador;
import entidades.Partida;
import vistas.Juego;
import vistas.ListaPartidas;
import errores.ValidationError;

public class InicioCtrl {
	
	ColeccionPartidas coleccionPartidas = new ColeccionPartidas();
	ColeccionJugadores coleccionJugadores = new ColeccionJugadores();

	// Devuelve un arreglo de id de las partidas 
	// pasando 2 dni de jugadores
	public void iniciar(String dniText1, String dniText2) throws ValidationError{
		
		// Validar campos vacios
		if(dniText1.contentEquals("") | dniText2.contentEquals("")){
			throw new ValidationError("Uno de los dos está vacío.");
		}
				
				
		// Validar int
		int dni1, dni2;
		try {
			dni1 = Integer.parseInt(dniText1);
			dni2 = Integer.parseInt(dniText2);
		} catch (Exception e) {
			throw new ValidationError("Uno de los dos no es un entero.");
		}
		
		// Validar que no sean iguales
		if (dni1 == dni2) {
			throw new ValidationError("Los DNI son iguales.");
		}
	
		// Validar jugadores existentes
		Jugador jugador1 = coleccionJugadores.getJugadorByDNI(dni1);
		Jugador jugador2 = coleccionJugadores.getJugadorByDNI(dni2);
		
		if (jugador1 == null || jugador2 == null) {
			throw new ValidationError("Uno de los dos DNI no está en la DB.");
		}
		
		// Todo OK! Proceed.
		
		ArrayList<Partida> partidas = hayPartidas(jugador1, jugador2);
		
		if (partidas == null) {
			try {
				Juego partida = new Juego(coleccionPartidas.crearNuevaPartida(jugador1,jugador2));
				partida.frame.setVisible(true);
				iniciarNuevaPartida(jugador1,jugador2);
			} catch (Exception e) {
				e.printStackTrace();
			}
		}
		else {
			try {
				ListaPartidas listaPartida = new ListaPartidas(partidas,jugador1, jugador2);	
				listaPartida.frmAjedrezV.setVisible(true);
			} catch (Exception e) {
				e.printStackTrace();
			}
		}
	}

	private ArrayList<Partida> hayPartidas(Jugador j1, Jugador j2) {
		return coleccionPartidas.getPartidas(j1.getdNI(),j2.getdNI());
	}
	
	public void iniciarNuevaPartida(Jugador j1, Jugador j2) {
		// TODO Auto-generated method stub	
		try {
			Juego juego = new Juego(coleccionPartidas.crearNuevaPartida(j1, j2));
			juego.frame.setVisible(true);
		} catch (Exception e) {
			e.printStackTrace();
		}
	}


}