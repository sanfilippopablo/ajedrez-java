package vistas;

import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import java.util.ArrayList;

import javax.swing.AbstractListModel;
import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JList;
import javax.swing.JSeparator;

import negocio.ListaPartidasCtrl;
import entidades.Jugador;
import entidades.Partida;
import errores.ValidationError;
import javax.swing.SwingConstants;
import java.awt.Color;
import java.awt.Font;

public class ListaPartidas {

	public JFrame frmAjedrezV;
	public JList<Integer> list;
	public Integer partidaSelec;
	JLabel lblError;
	public ListaPartidasCtrl listaPartidasCtrl= new ListaPartidasCtrl();
	public Jugador j1,j2;
	
	/**
	 * Create the application.
	 */
	public ListaPartidas(ArrayList<Partida> partidas,Jugador j1, Jugador j2) {
		this.j1 = j1;
		this.j2 = j2;
		initialize(partidas);
	}

	/**
	 * Inicializa la lista de partidas entre dos jugadores.
	 */
	public void initialize(ArrayList<Partida> partidas) {
		frmAjedrezV = new JFrame();
		frmAjedrezV.setTitle("Ajedrez V0.01");
		frmAjedrezV.setResizable(false);
		frmAjedrezV.setBounds(100, 100, 260, 380);
		frmAjedrezV.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		frmAjedrezV.getContentPane().setLayout(null);
		
		JSeparator separator = new JSeparator();
		separator.setBounds(189, 35, 1, 106);
		frmAjedrezV.getContentPane().add(separator);
		
		JSeparator separator_1 = new JSeparator();
		separator_1.setBounds(24, 260, 208, 12);
		frmAjedrezV.getContentPane().add(separator_1);
		
		JButton btnNewButton = new JButton("Nueva partida");
		btnNewButton.addMouseListener(new MouseAdapter() {
			@Override
			public void mouseClicked(MouseEvent arg0) {
				iniciarNuevaPartida();
			}
		});
		btnNewButton.setBounds(66, 276, 117, 29);
		frmAjedrezV.getContentPane().add(btnNewButton);
		
		JButton btnNewButton_1 = new JButton("Cargar partida");
		
		btnNewButton_1.addMouseListener(new MouseAdapter() {

			@Override
			public void mouseClicked(MouseEvent arg0) {
				iniciarPartida();
			}
		});
		btnNewButton_1.setBounds(66, 230, 117, 29);
		frmAjedrezV.getContentPane().add(btnNewButton_1);
		
		list = new JList<Integer>();
		list.setModel(new AbstractListModel<Integer>() {
			private static final long serialVersionUID = 1L;
			
			public int getSize() {
				return partidas.size();
			}
			public Integer getElementAt(int index) {
				return partidas.get(index).getId();
			}
		});
		list.setBounds(24, 48, 208, 170);
		
		frmAjedrezV.getContentPane().add(list);
		
		JLabel lblSeleccioneUnaPartida = new JLabel("Seleccione una partida");
		lblSeleccioneUnaPartida.setFont(new Font("Tahoma", Font.BOLD, 11));
		lblSeleccioneUnaPartida.setBounds(24, 20, 208, 16);
		frmAjedrezV.getContentPane().add(lblSeleccioneUnaPartida);
		
		lblError = new JLabel("");
		lblError.setForeground(Color.RED);
		lblError.setHorizontalAlignment(SwingConstants.CENTER);
		lblError.setBounds(24, 316, 208, 35);
		frmAjedrezV.getContentPane().add(lblError);
	}
	
	/*
	 * Inicia una partida guardada con anterioridad
	 * de los jugadores previamente cargados
	 */
	private void iniciarPartida(){
		
		try {
			partidaSelec = list.getSelectedValue();
			listaPartidasCtrl.iniciarPartida(partidaSelec);
			frmAjedrezV.setVisible(false);
		} catch (ValidationError e) {
			lblError.setText(e.getMessage());
		}
	}
	
	/*
	 * Inicia una partida nueva entre los dos jugadores previamente cargados.
	 */
	private void iniciarNuevaPartida(){

		frmAjedrezV.setVisible(false);
		listaPartidasCtrl.iniciarNuevaPartida(j1,j2);
	}

}
