package vistas;

import java.util.ArrayList;

import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JTable;
import javax.swing.JTextField;
import javax.swing.SwingConstants;
import javax.swing.table.DefaultTableModel;

import negocio.JuegoCtrl;
import entidades.Partida;
import entidades.Pieza;
import errores.ValidationError;

import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import java.awt.Font;

public class Juego {

	public JFrame frame;
	private JTable table;
	private JTextField txtPieza;
	private JTextField txtPosY;
	private JLabel errorLabel;
	private JLabel lblTurno;
	public ArrayList<Pieza> piezas = new ArrayList<Pieza>();
	public JuegoCtrl juegoCtrl = new JuegoCtrl();
	private JTextField txtPosX;
	private Partida partida;

	/**
	 * Create the application.
	 */
	public Juego(Partida partida) {
		this.partida = partida;
		initialize();
	}

	/**
	 * Initialize the contents of the frame.
	 */
	private void initialize() {
		frame = new JFrame();
		frame.setResizable(false);
		frame.setBounds(100, 100, 588, 585);
		frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		
		frame.getContentPane().setLayout(null);
		table = new JTable();
		table.setBounds(25, 27, 361, 518);
		frame.getContentPane().add(table);
		renderTable();

		JLabel lblTituloTurno = new JLabel("Turno");
		lblTituloTurno.setFont(new Font("Tahoma", Font.BOLD, 15));
		lblTituloTurno.setBounds(458, 27, 61, 16);
		lblTituloTurno.setHorizontalAlignment(SwingConstants.CENTER);
		frame.getContentPane().add(lblTituloTurno);

		lblTurno = new JLabel("");
		lblTurno.setBounds(458, 73, 61, 16);
		lblTurno.setHorizontalAlignment(SwingConstants.CENTER);
		frame.getContentPane().add(lblTurno);
		renderTurno();


		txtPieza = new JTextField();
		txtPieza.setBounds(418, 250, 144, 28);
		frame.getContentPane().add(txtPieza);
		txtPieza.setColumns(10);

		txtPosY = new JTextField();
		txtPosY.setBounds(501, 316, 61, 28);
		frame.getContentPane().add(txtPosY);
		txtPosY.setColumns(10);

		JButton btnMover = new JButton("Mover");
		btnMover.addMouseListener(new MouseAdapter() {
			@Override
			public void mouseClicked(MouseEvent arg0) {
				moverPieza();
			}
		});
		btnMover.setBounds(418, 365, 144, 29);
		frame.getContentPane().add(btnMover);

		JLabel lblPieza = new JLabel("Pieza");
		lblPieza.setFont(new Font("Tahoma", Font.BOLD, 11));
		lblPieza.setHorizontalAlignment(SwingConstants.CENTER);
		lblPieza.setBounds(458, 222, 61, 16);
		frame.getContentPane().add(lblPieza);

		JLabel lblPosY = new JLabel("Pos. Y");
		lblPosY.setFont(new Font("Tahoma", Font.BOLD, 11));
		lblPosY.setBounds(501, 289, 61, 16);
		frame.getContentPane().add(lblPosY);

		txtPosX = new JTextField();
		txtPosX.setColumns(10);
		txtPosX.setBounds(418, 316, 61, 28);
		frame.getContentPane().add(txtPosX);

		JLabel lblPosX = new JLabel("Pos. X");
		lblPosX.setFont(new Font("Tahoma", Font.BOLD, 11));
		lblPosX.setBounds(418, 289, 61, 16);
		frame.getContentPane().add(lblPosX);
		
		errorLabel = new JLabel("");
		errorLabel.setBounds(418, 101, 144, 76);
		frame.getContentPane().add(errorLabel);
		
		JLabel lblJugador = new JLabel("Jugador");
		lblJugador.setFont(new Font("Tahoma", Font.BOLD, 11));
		lblJugador.setBounds(37, 11, 61, 14);
		frame.getContentPane().add(lblJugador);
		
		JLabel lblPieza_1 = new JLabel("Pieza");
		lblPieza_1.setFont(new Font("Tahoma", Font.BOLD, 11));
		lblPieza_1.setBounds(121, 11, 46, 14);
		frame.getContentPane().add(lblPieza_1);
		
		JLabel lblPosicionX = new JLabel("Posicion X");
		lblPosicionX.setFont(new Font("Tahoma", Font.BOLD, 11));
		lblPosicionX.setBounds(213, 11, 89, 14);
		frame.getContentPane().add(lblPosicionX);
		
		JLabel lblPosicionY = new JLabel("Posicion Y");
		lblPosicionY.setFont(new Font("Tahoma", Font.BOLD, 11));
		lblPosicionY.setBounds(311, 11, 75, 14);
		frame.getContentPane().add(lblPosicionY);
	}

	public void moverPieza(){
		try {
			juegoCtrl.moverPieza(partida, txtPieza.getText(), txtPosX.getText(), txtPosY.getText());
			
			if (partida.estaTerminada()) {
				JOptionPane.showMessageDialog(null, "Partida terminada");
				frame.setVisible(false);
				Inicio window = new Inicio();
				window.frmAjedrezV.setVisible(true);
			}
			
			//Rerender UI
			renderTurno();
			renderTable();
		} catch (ValidationError e) {
			errorLabel.setText(e.getMessage());
			
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}
	
	private void renderTurno(){
		lblTurno.setText(partida.getTurnoActual().getApellido());
	}
	
	private void renderTable(){
		piezas = partida.getPiezas();
		String Objeto[][] = new String[piezas.size()][4];
		for(int i = 0;i < piezas.size();i++){
			Objeto[i][0]= piezas.get(i).getJugador().getApellido();
			Objeto[i][1] = piezas.get(i).getId();
			Objeto[i][2]= Integer.toString(piezas.get(i).getPosX());
			Objeto[i][3]= Integer.toString(piezas.get(i).getPosY());
		}
		table.setModel(new DefaultTableModel(
				Objeto,
				new String[] {
					"Jugador", "Pieza", "X", "Y"
				}
			));
		
	}
}
