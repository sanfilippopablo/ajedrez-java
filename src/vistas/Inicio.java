package vistas;

import java.awt.Color;
import java.awt.EventQueue;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;

import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JTextField;
import javax.swing.SwingConstants;

import errores.ValidationError;
import negocio.InicioCtrl;
import java.awt.Font;

public class Inicio {

	public JFrame frmAjedrezV;
	private JTextField inDni1;
	private JTextField inDni2;
	private JLabel lblDniJugador_1;
	private JLabel lblError;

	private InicioCtrl inicioCtrl = new InicioCtrl();
	
	/**
	 * Launch the application.
	 */
	public static void main(String[] args) {
		EventQueue.invokeLater(new Runnable() {
			public void run() {
				try {
					Inicio window = new Inicio();
					window.frmAjedrezV.setVisible(true);
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});
	}

	/**
	 * Create the application.
	 */
	public Inicio() {
		initialize();
	}

	/**
	 * Initialize the contents of the frame.
	 */
	private void initialize() {

		frmAjedrezV = new JFrame();
		frmAjedrezV.setResizable(false);
		frmAjedrezV.setTitle("Ajedrez V0.01");
		frmAjedrezV.setBounds(100, 100, 248, 336);
		frmAjedrezV.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		frmAjedrezV.getContentPane().setLayout(null);
		
		inDni1 = new JTextField();
		inDni1.setBounds(62, 70, 134, 28);
		frmAjedrezV.getContentPane().add(inDni1);
		inDni1.setColumns(10);
		
		JLabel lblDniJugador = new JLabel("DNI Jugador 1");
		lblDniJugador.setFont(new Font("Tahoma", Font.BOLD, 11));
		lblDniJugador.setHorizontalAlignment(SwingConstants.CENTER);
		lblDniJugador.setBounds(62, 42, 134, 16);
		frmAjedrezV.getContentPane().add(lblDniJugador);
		
		inDni2 = new JTextField();
		inDni2.setColumns(10);
		inDni2.setBounds(62, 138, 134, 28);
		frmAjedrezV.getContentPane().add(inDni2);
		
		lblDniJugador_1 = new JLabel("DNI Jugador 2");
		lblDniJugador_1.setFont(new Font("Tahoma", Font.BOLD, 11));
		lblDniJugador_1.setHorizontalAlignment(SwingConstants.CENTER);
		lblDniJugador_1.setBounds(62, 110, 134, 16);
		frmAjedrezV.getContentPane().add(lblDniJugador_1);
		
		JButton btnIniciar = new JButton("Iniciar");
		btnIniciar.addMouseListener(new MouseAdapter() {
			@Override
			public void mouseClicked(MouseEvent e) {
				iniciar();
			}

		});
		btnIniciar.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
			}
		});
		btnIniciar.setBounds(62, 198, 134, 29);
		frmAjedrezV.getContentPane().add(btnIniciar);
		
		lblError = new JLabel("");
		lblError.setForeground(Color.RED);
		lblError.setHorizontalAlignment(SwingConstants.CENTER);
		lblError.setBounds(6, 239, 236, 69);
		frmAjedrezV.getContentPane().add(lblError);

	}
	
	/*
	 * Ingreso de los jugadores al juego
	 */
	private void iniciar(){
		
		try {
			inicioCtrl.iniciar(inDni1.getText(), inDni2.getText());
			frmAjedrezV.setVisible(false);
		} catch (ValidationError e) {
			lblError.setText(e.getMessage());
		}

	}

}
