-- MySQL dump 10.15  Distrib 10.0.21-MariaDB, for Linux (x86_64)
--
-- Host: localhost    Database: Ajedrez
-- ------------------------------------------------------
-- Server version	10.0.21-MariaDB-log

CREATE DATABASE IF NOT EXISTS Ajedrez;
USE Ajedrez;

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+00:00' */;
/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;

--
-- Table structure for table `jugadores`
--

DROP TABLE IF EXISTS `jugadores`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `jugadores` (
  `dni` int(11) NOT NULL,
  `nombre` tinytext,
  `apellido` tinytext,
  PRIMARY KEY (`dni`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `jugadores`
--

LOCK TABLES `jugadores` WRITE;
/*!40000 ALTER TABLE `jugadores` DISABLE KEYS */;
INSERT INTO `jugadores` VALUES (123,'Pablo','Sanfilippo'),(456,'Facundo','Segarra'),(38292089,'Gonzalo','Perez Fariña'),(789, 'Sebastián', 'Baskovich'),(321,'Ernesto','Bugdahl'),(654,'Facundo','Pascual');
/*!40000 ALTER TABLE `jugadores` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `partidas`
--

DROP TABLE IF EXISTS `partidas`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `partidas` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `jugador1` int(11) NOT NULL,
  `jugador2` int(11) NOT NULL,
  `turno` int(11) NOT NULL,
  PRIMARY KEY (`id`),
  KEY `jugador1` (`jugador1`),
  KEY `jugador2` (`jugador2`),
  KEY `turno` (`turno`),
  CONSTRAINT `jugador1fk` FOREIGN KEY (`jugador1`) REFERENCES `jugadores` (`dni`),
  CONSTRAINT `jugador2fk` FOREIGN KEY (`jugador2`) REFERENCES `jugadores` (`dni`),
  CONSTRAINT `turnofk` FOREIGN KEY (`turno`) REFERENCES `jugadores` (`dni`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `partidas`
--

LOCK TABLES `partidas` WRITE;
/*!40000 ALTER TABLE `partidas` DISABLE KEYS */;
INSERT INTO `partidas` VALUES (1,38292089,123,123),(2,38292089,123,38292089),(3,123,456,456),(4,456,38292089,456),(5,456,123,123),(6,456,123,123);
/*!40000 ALTER TABLE `partidas` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `piezas`
--

DROP TABLE IF EXISTS `piezas`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `piezas` (
  `jugador` int(11) NOT NULL,
  `partida` int(11) NOT NULL,
  `id` varchar(2) NOT NULL,
  `posx` int(1) DEFAULT NULL,
  `posy` int(1) DEFAULT NULL,
  PRIMARY KEY (`id`,`partida`,`jugador`),
  KEY `jugador` (`jugador`),
  KEY `partida` (`partida`),
  CONSTRAINT `jugadorfk` FOREIGN KEY (`jugador`) REFERENCES `jugadores` (`dni`),
  CONSTRAINT `partidafk` FOREIGN KEY (`partida`) REFERENCES `partidas` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `piezas`
--

LOCK TABLES `piezas` WRITE;
/*!40000 ALTER TABLE `piezas` DISABLE KEYS */;
INSERT INTO `piezas` VALUES (456,5,'A1',3,3),(123,1,'A2',6,7),(456,6,'C2',3,4),(123,1,'D1',5,8),(38292089,1,'D1',5,3),(38292089,2,'D1',2,2),(456,4,'D1',6,5),(456,5,'D1',1,2),(456,5,'P2',5,6),(123,5,'P3',1,7),(38292089,1,'P4',4,3),(456,5,'P4',6,5),(456,5,'P5',2,2),(38292089,1,'P6',6,4),(123,1,'R1',4,8),(38292089,1,'R1',4,2),(456,2,'R1',8,1),(38292089,2,'R1',1,1),(123,3,'R1',2,2),(456,3,'R1',7,7),(456,4,'R1',2,5),(38292089,4,'R1',7,7),(123,5,'R1',1,8),(456,5,'R1',1,3),(123,6,'R1',8,8),(456,6,'R1',4,1),(123,1,'T1',8,8),(123,3,'T2',8,3);
/*!40000 ALTER TABLE `piezas` ENABLE KEYS */;
UNLOCK TABLES;
/*!40103 SET TIME_ZONE=@OLD_TIME_ZONE */;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;

-- Dump completed on 2015-10-08 16:03:38
